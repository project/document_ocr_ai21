# Document OCR AI21 Studio

Document OCR integration with AI21 Studio. AI21 Studio is a platform that provides developers
and businesses with top-tier natural language processing (NLP) solutions

For a full description of the module, visit the
[project page](https://www.drupal.org/project/document_ocr_ai21).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/document_ocr_ai21).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the Document OCR module (https://www.drupal.org/project/document_ocr)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configure the Document OCR AI21 Studio at (/admin/config/structure/document-ocr).

### [AI21 Studio](https://www.ai21.com/)

1. Get an API Key [here](https://studio.ai21.com/account/api-key)
2. Create JSON file and store your credentials in the following format:
```
{
  "apikey": "[API-KEY]"
}
```
4. Upload it to the private directory `private://document-ocr/ai21-credentials.json`
5. Point to the credentials file when adding AI21 specific transformer plugins
6. You should be all set to use AI21 Studio integration

## Maintainers

- [Minnur Yunusov (minnur)](https://www.drupal.org/u/minnur)
