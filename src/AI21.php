<?php

namespace Drupal\document_ocr_ai21;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Client;

/**
 * AI21 integration service class.
 */
class AI21 {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * API Endpoint.
   */
  protected $api_endpoint = 'https://api.ai21.com/studio/v1';

  /**
   * Constructor.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger service.
   */
  public function __construct(Client $http_client, LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $http_client;
    $this->loggerFactory = $logger_factory->get('document_ocr_ai21');
  }

  /**
   * Set JSON credentials file.
   */
  public function setCredentials($credentials) {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * Text summarizer.
   */
  public function getSummary($source_type, $value, $focus) {
    $payload = [
      'sourceType' => $source_type,
      'source' => $value,
    ];
    if (!empty($focus)) {
      $payload['focus'] = $focus;
    }
    return $this->callApi('summarize', $payload);
  }

  /**
   * Text Segmentation.
   */
  public function getSegmentedText($source_type, $value) {
    return $this->callApi('segmentation', [
      'sourceType' => $source_type,
      'source' => $value,
    ]);
  }

  /**
   * Make an API call.
   */
  protected function callApi($api, $payload) {
    try {
      $request = $this->httpClient->post($this->api_endpoint . '/' . $api, [
        'headers' => [
          'Authorization' => 'Bearer ' . $this->credentials['apikey'],
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'body' => json_encode($payload)
      ]);
      $response = json_decode($request->getBody()->getContents(), true);
      if (!empty($response['detail'])) {
        throw new \Exception($response['detail']);
      }
      else {
        return $response;
      }
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

}
