<?php

namespace Drupal\document_ocr_ai21\Plugin\document_ocr\transformer;

use Drupal\Core\Form\FormStateInterface;

/**
 * AI21 Summarize API integration transformer plugin.
 *
 * @DocumentOcrTransformer(
 *   id = "ai21_summarize",
 *   name = @Translation("AI21 Summarize"),
 *   group = @Translation("AI21"),
 *   description = @Translation("AI21 Summarize API. The Summarize API only supports English inputs at this time."),
 *   requires = {
 *     "credentials"
 *   }
 * )
 */
class AI21Summarize extends AI21Base {

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $form['focus'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Focus (optional)'),
      '#description' => $this->t('Return only summaries focused on a topic of your choice. Up to 50 characters.'),
      '#max_length' => 50,
      '#default_value' => !empty($this->configuration['focus']) ? $this->configuration['focus'] : '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'focus' => $form_state->getValue('focus'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    try {
      $response = $this->service->setCredentials($this->getCredentials())
        ->getSummary('TEXT', $value, $this->configuration['focus']);
      return !empty($response['summary']) ? $response['summary'] : '';
    }
    catch (\Exception $ex) {
      return $value;
    }
  }

}
