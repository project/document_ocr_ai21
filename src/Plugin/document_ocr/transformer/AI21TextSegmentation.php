<?php

namespace Drupal\document_ocr_ai21\Plugin\document_ocr\transformer;

/**
 * AI21 Text Segmentation API integration transformer plugin.
 *
 * @DocumentOcrTransformer(
 *   id = "ai21_text_segmentation",
 *   name = @Translation("AI21 Text Segmentation"),
 *   group = @Translation("AI21"),
 *   description = @Translation("AI21 Text Segmentation API (break text into paragraphs)."),
 *   requires = {
 *     "credentials"
 *   }
 * )
 */
class AI21TextSegmentation extends AI21Base {

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    try {
      $response = $this->service->setCredentials($this->getCredentials())
        ->getSegmentedText('TEXT', $value);
      $segments = [];
      if (!empty($response['segments'])) {
        foreach ($response['segments'] as $segment) {
          $segments[] = $segment['segmentText'];
        }
      }
      return join("\n\n", $segments);
    }
    catch (\Exception $ex) {
      return $value;
    }
  }

}
