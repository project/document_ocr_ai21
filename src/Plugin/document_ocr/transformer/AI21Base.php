<?php

namespace Drupal\document_ocr_ai21\Plugin\document_ocr\transformer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\Plugin\TransformerBase;
use Drupal\document_ocr_ai21\AI21;

/**
 * AI21 Base transformer class.
 */
class AI21Base extends TransformerBase {

  /**
   * AI21 serivce.
   *
   * @var \Drupal\document_ocr_ai21\AI21
   */
  protected $service;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger_factory, AI21 $ai21) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $file_system, $logger_factory);
    $this->service = $ai21;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('document_ocr_ai21.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return TRUE;
  }

}
